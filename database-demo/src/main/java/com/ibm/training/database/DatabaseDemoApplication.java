package com.ibm.training.database;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ibm.training.database.entity.Employee;
import com.ibm.training.database.repository.EmployeeRepository;

@SpringBootApplication
@ComponentScan(basePackages= {"com.ibm.training.database.*", "com.ibm.training.database.repository"})
@EntityScan(basePackages= {"com.ibm.training.database.entity" })
@EnableJpaRepositories(basePackages= {"com.ibm.training.database.repository" })
public class DatabaseDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseDemoApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(EmployeeRepository employeeRepository) {
		return (args)->{
			Employee employee = new Employee();
			employee.setLoginName("aaa");
			employeeRepository.save(employee);
		};
	}

}

