/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.entity.service;

import com.ibm.entity.Role;
import com.ibm.repository.RoleRepository;
import java.util.List;
import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;

/**
 *
 * @author avincze
 */
@Stateless
@LocalBean
public class RoleService {

    @EJB
    private RoleRepository roleRepository;
    
    @Resource
    private SessionContext sessionContext;
    
    //@RolesAllowed("ROLE_VALAKI")
    public List<Role> getAll(){
        System.out.println(sessionContext.getCallerPrincipal().getName());
        return roleRepository.findAll();
    }
}
