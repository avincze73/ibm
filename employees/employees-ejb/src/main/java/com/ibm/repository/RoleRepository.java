/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.repository;

import com.ibm.entity.Role;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author martin
 */
@Stateless
@LocalBean
public class RoleRepository {

    @PersistenceContext(unitName = "ibmTPU")
    private EntityManager em;
    
    
    public List<Role> findAll(){
        return em.createQuery("select r from Role r", Role.class).getResultList();
    }
    

}
