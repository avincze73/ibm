/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.auth.cli;

import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 *
 * @author avincze
 */
public class Main {
    
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/auth");
        target.register(new HTTPBasicAuthFilter("admin", "admin"));
        Response result =  target.path("webresources").path("authentication").path("echo").request().get();
        System.out.println(result);
    }
    
}
