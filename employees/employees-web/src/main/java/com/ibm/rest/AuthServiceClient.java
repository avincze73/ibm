/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.rest;

import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

/**
 * Jersey REST client generated for REST resource:AuthenticationResource
 * [authentication]<br>
 * USAGE:
 * <pre>
 *        AuthServiceClient client = new AuthServiceClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author avincze
 */
public class AuthServiceClient {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/auth/webresources";

    public AuthServiceClient() {
        ClientConfig config = new ClientConfig();
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "admin");
        client = javax.ws.rs.client.ClientBuilder.newClient(config);
        client.register(feature);
        webTarget = client.target(BASE_URI).path("authentication");
    }

    public AuthServiceClient(String username, String password) {
        this();
        setUsernamePassword(username, password);
    }

    public void putJson(Object requestEntity) throws ClientErrorException {
        webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).put(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON));
    }

    public String echo() throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path("echo");
        System.out.println(resource.getUri().getPath());
        System.out.println(resource.getUri().getPath());
        return resource.request(javax.ws.rs.core.MediaType.TEXT_PLAIN).get(String.class);
    }

    public <T> T getJson_XML(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public <T> T getJson_JSON(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    public void close() {
        client.close();
    }

    public final void setUsernamePassword(String username, String password) {

        //webTarget.register(new HTTPBasicAuthFilter(username, password));
    }

}
