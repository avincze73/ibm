/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.controller;

import com.ibm.rest.AuthServiceClient;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author avincze
 */
@Named(value = "mainController")
@SessionScoped
public class MainController implements Serializable {

    private String welcomeMessage;
    /**
     * Creates a new instance of MainController
     */
    public MainController() {
        this.welcomeMessage = "Welcome Message";
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomMessage) {
        this.welcomeMessage = welcomMessage;
    }
    
    public String buttonAction(){
        return "roles.jsf?faces-redirect=true";
    }
    
    
    
     public void restCallAction(){
         try {
             AuthServiceClient authServiceClient = new AuthServiceClient();
             System.out.println(authServiceClient.echo());
             
         } catch (Exception e) {
             Logger.getLogger("rest").log(Level.SEVERE, e.getMessage());
         }
         
         
    }
    
}
