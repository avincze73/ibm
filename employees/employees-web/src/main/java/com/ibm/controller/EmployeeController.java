/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm.controller;

import com.ibm.entity.Employee;
import com.ibm.entity.Role;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author avincze
 */
@Named(value = "employeeController")
@SessionScoped
public class EmployeeController implements Serializable {

    @EJB
    private com.ibm.entity.service.RoleService roleService;

    private List<Employee> employeeList;

    private List<Role> roleList;

    private Employee newEmployee;

    private boolean saveActive;
    private boolean addActive;
    private boolean cancelActive;

    /**
     * Creates a new instance of EmployeeController
     */
    public EmployeeController() {
    }

    @PostConstruct
    protected void init() {
        this.roleList = roleService.getAll();
        
        this.newEmployee = new Employee();

        this.saveActive = false;
        this.addActive = true;
        this.cancelActive = false;

        employeeList = new ArrayList<>();
        Employee employee = new Employee();
        employee.setId(1L);
        employee.setFirstName("firstName1");
        employee.setLastName("lastName1");
        employee.setLoginName("loginName1");
        employee.setPassword("password1");
        employee.setTitle("title1");
        employeeList.add(employee);

        employee = new Employee();
        employee.setId(2L);
        employee.setFirstName("firstName2");
        employee.setLastName("lastName2");
        employee.setLoginName("loginName2");
        employee.setPassword("password2");
        employee.setTitle("title2");
        employeeList.add(employee);

        employee = new Employee();
        employee.setId(3L);
        employee.setFirstName("firstName3");
        employee.setLastName("lastName3");
        employee.setLoginName("loginName3");
        employee.setPassword("password3");
        employee.setTitle("title3");
        employeeList.add(employee);

    }

    public void addAction() {
        this.newEmployee = new Employee();
        this.addActive = false;
        this.saveActive = true;
        this.cancelActive = true;

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(null, new FacesMessage("Successful", "Your message: added"));
    }

    public void saveAction() {
        employeeList.add(newEmployee);
        this.addActive = true;
        this.saveActive = false;
        this.cancelActive = false;

    }

    public void cancelAction() {

    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    public Employee getNewEmployee() {
        return newEmployee;
    }

    public void setNewEmployee(Employee newEmployee) {
        this.newEmployee = newEmployee;
    }

    public boolean isSaveActive() {
        return saveActive;
    }

    public void setSaveActive(boolean saveActive) {
        this.saveActive = saveActive;
    }

    public boolean isAddActive() {
        return addActive;
    }

    public void setAddActive(boolean addActive) {
        this.addActive = addActive;
    }

    public boolean isCancelActive() {
        return cancelActive;
    }

    public void setCancelActive(boolean cancelActive) {
        this.cancelActive = cancelActive;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }
    
    

}
