/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




var app = angular.module('demo', ['ngResource']);


app.controller('mainController', ['$scope', '$resource', '$filter', function ($scope, $resource, $filter) {
    $scope.welcome = "Üdvözöljük";
    $scope.counter = 1;
    $scope.users = {};
    $scope.loading = false;
    
    var messages = $resource("http://localhost:8080/auth/webresources/authentication/list");
    
     $scope.myClick = function () {
        console.log('search is called ' + $scope.welcome);
        $scope.welcome = "hello world." + ++$scope.counter;
    };
    
    
     $scope.restCall = function () {
         $scope.loading = true;
       $scope.users = messages.query();
        $scope.users.$promise.then(function (response) {
            console.log('success');
            $scope.loading = false;
            $('#userList').DataTable();
        }, function (error) {
            console.log('error');
            $scope.loading = false;
        });
    };
   
    
    
}])