/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ibm;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author avincze
 */
@Path("authentication")
@RequestScoped
public class AuthenticationResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AuthenticationResource
     */
    public AuthenticationResource() {
    }

    /**
     * Retrieves representation of an instance of com.ibm.AuthenticationResource
     * @return an instance of java.lang.String
     */
    
    @GET
    @Path(value = "echo")
    @Produces(MediaType.TEXT_PLAIN)
    public String echo() {
        //TODO return proper representation object
        return "echo";
    }
    
    
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public MyUser getJson() {
        //TODO return proper representation object
        return new MyUser("a", "b");
    }
    
    
    
     @GET
     @Path(value = "list")
    @Produces({MediaType.APPLICATION_JSON})
    public List<MyUser> getUsers() {
        //TODO return proper representation object
        List<MyUser> result = new ArrayList<>();
        result.add(new  MyUser("a", "b"));
        result.add(new  MyUser("a", "b"));
        result.add(new  MyUser("a", "b"));
        result.add(new  MyUser("a", "b"));
        result.add(new  MyUser("a", "b"));
        
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(AuthenticationResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * PUT method for updating or creating an instance of AuthenticationResource
     * @param content representation for the resource
     */
   
}
