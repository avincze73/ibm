package com.ibm.training.intro;

public interface GreetingService {
	String greeting();
	int getCount();

}
