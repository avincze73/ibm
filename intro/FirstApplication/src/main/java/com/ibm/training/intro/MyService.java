package com.ibm.training.intro;

public class MyService {
	
	private MyRepository myRepository;
	private MyLogger myLogger;
	private int a;
	private String b;
	private GreetingService greetingService;
	
	public MyService(MyLogger myLogger, int a) {
		super();
		this.myLogger = myLogger;
		this.a = a;
	}

	public void save() {
		myRepository.save();
		System.out.println(greetingService.greeting());
		
	}

	public MyRepository getMyRepository() {
		return myRepository;
	}

	public void setMyRepository(MyRepository myRepository) {
		this.myRepository = myRepository;
	}

	public GreetingService getGreetingService() {
		return greetingService;
	}

	public void setGreetingService(GreetingService greetingService) {
		this.greetingService = greetingService;
	}
	
	
	
	

}
